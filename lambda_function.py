import json
import boto3

def lambda_handler(event, context):
    for record in event['Records']:
        bucket = record['s3']['bucket']['name']
        fileName = record['s3']['object']['key'] 
        
        client=boto3.client('rekognition')
        
        response = client.detect_labels(Image={'S3Object':{'Bucket':bucket,'Name':fileName}})
        
        string = ""
        
        splittedFileName = fileName.split("/")
        
        s3_path = splittedFileName[0] + "/result/" + splittedFileName[1] + "-analyzed.txt"
    
        s3 = boto3.resource("s3")
        
        print('Detected labels for ' + fileName)    
        
        for label in response['Labels']:
            string = string + label['Name'] + ' : ' + str(label['Confidence']) + "\n"
            
        encoded_string = string.encode("utf-8")
        
        s3.Bucket(bucket).put_object(Key=s3_path, Body=encoded_string)
